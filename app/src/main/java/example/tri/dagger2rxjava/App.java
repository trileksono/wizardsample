package example.tri.dagger2rxjava;

import android.app.Application;
import example.tri.dagger2rxjava.net.API;
import example.tri.dagger2rxjava.net.APIFactory;
import io.paperdb.Paper;
import timber.log.Timber;

/**
 * Created by tri on 2/11/17.
 */

public class App extends Application {

  private static App instance;
  private API mApi;

  @Override public void onCreate() {
    super.onCreate();
    Paper.init(this); // init PaperDB

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree() {
        @Override protected String createStackElementTag(StackTraceElement element) {
          return super.createStackElementTag(element) + " : " + element.getLineNumber();
        }
      });
    }
    instance = this;
  }

  public static App getInstance() {
    return instance;
  }

  public API getApi(String URL) {
    if (mApi == null) {
      mApi = new APIFactory().getApi(this,URL);
    }
    return mApi;
  }
}
