package example.tri.dagger2rxjava.ui.dua;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import example.tri.dagger2rxjava.base.BaseFragment;
import example.tri.dagger2rxjava.ui.fragment.FragmentTiga;
import java.util.List;

/**
 * Created by tri on 2/18/17.
 */

public class DuaAdapter extends FragmentStatePagerAdapter {

  private List<BaseFragment> listFragment;

  public DuaAdapter(FragmentManager fm, List<BaseFragment> listFragment) {
    super(fm);
    this.listFragment = listFragment;
  }

  @Override public Fragment getItem(int position) {
    if (position >= listFragment.size()) {
      return new FragmentTiga();
    }
    return listFragment.get(position);
  }

  @Override public int getCount() {
    return listFragment.size();
  }

  @Override public int getItemPosition(Object object) {
    return POSITION_NONE;
  }
}