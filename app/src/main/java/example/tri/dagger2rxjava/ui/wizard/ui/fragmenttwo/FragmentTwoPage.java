package example.tri.dagger2rxjava.ui.wizard.ui.fragmenttwo;

import android.support.v4.app.Fragment;
import example.tri.dagger2rxjava.ui.wizard.model.ModelCallbacks;
import example.tri.dagger2rxjava.ui.wizard.model.Page;
import example.tri.dagger2rxjava.ui.wizard.model.ReviewItem;
import java.util.ArrayList;

/**
 * Created by tri on 2/20/17.
 */

public class FragmentTwoPage extends Page {
  public FragmentTwoPage(ModelCallbacks callbacks, String title) {
    super(callbacks, title);
  }

  @Override public Fragment createFragment() {
    return new FragmentTwo();
  }

  @Override public boolean isCompleted() {
    return super.isCompleted();
  }

  @Override public void getReviewItems(ArrayList<ReviewItem> dest) {

  }
}
