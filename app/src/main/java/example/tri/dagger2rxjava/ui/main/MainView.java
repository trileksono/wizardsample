package example.tri.dagger2rxjava.ui.main;

import example.tri.dagger2rxjava.base.BaseView;
import example.tri.dagger2rxjava.data.LoginResponse;

/**
 * Created by tri on 2/11/17.
 */

public interface MainView extends BaseView {

  void onResponse(LoginResponse loginResponse);

  void onErrorResponse(String message);
}
