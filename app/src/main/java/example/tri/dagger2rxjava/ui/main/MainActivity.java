package example.tri.dagger2rxjava.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import butterknife.Bind;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseActivity;
import example.tri.dagger2rxjava.data.LoginResponse;

public class MainActivity extends BaseActivity implements MainView {

  @Bind(R.id.pager) ViewPager pager;

  private MainPresenter mainPresenter;

  @Override protected int getConctentView() {
    return R.layout.activity_main;
  }

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override protected void onViewReady(Bundle savedInstance) {
    mainPresenter = new MainPresenter();
    mainPresenter.onAttach(this);
  }

  @Override protected void onDestroy() {
    mainPresenter.onDettach();
    super.onDestroy();
  }

  @Override public void onResponse(LoginResponse loginResponse) {
    showToast(loginResponse.getMessage());
  }

  @Override public void onErrorResponse(String message) {
    showToast(message);
  }

  @Override public Context getContext() {
    return this;
  }

  @Override public void showLoading(String message) {
    showDialog(message);
  }

  @Override public void dismisLoading() {
    hideDialog();
  }
}
