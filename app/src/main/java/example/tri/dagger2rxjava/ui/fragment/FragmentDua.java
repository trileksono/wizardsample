package example.tri.dagger2rxjava.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.OnTextChanged;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseFragment;

/**
 * Created by tri on 2/15/17.
 */

public class FragmentDua extends BaseFragment {

  @Bind(R.id.txt_dua) EditText txtDua;

  @Override protected int getResourceLayout() {
    return R.layout.fragment_dua;
  }

  @Override protected void onViewReady(@Nullable Bundle savedInstanceState) {
  }

  @OnTextChanged(value = R.id.txt_dua,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED) void afterTextInput(Editable editable) {
    fieldComplete();
  }

  boolean fieldComplete() {
    if (txtDua.getText().toString().trim().length() > 0) {
      //EventBus.getDefault().post(new ValidationMessage(false, false, true));
      return true;
    }
    //EventBus.getDefault().post(new ValidationMessage(false, false, false));
    return false;
  }

  @Override public void setUserVisibleHint(boolean isVisibleToUser) {
    super.setUserVisibleHint(isVisibleToUser);
    if (isVisibleToUser) {
      fieldComplete();
    }
  }
}
