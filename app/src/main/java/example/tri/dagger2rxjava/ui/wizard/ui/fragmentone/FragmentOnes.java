package example.tri.dagger2rxjava.ui.wizard.ui.fragmentone;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.ButterKnife;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.ui.wizard.model.Page;
import example.tri.dagger2rxjava.ui.wizard.ui.PageFragmentCallbacks;

/**
 * Created by tri on 2/20/17.
 */

public class FragmentOnes extends Fragment {
  private static final String ARG_KEY = "key";

  @Bind(R.id.txt_satu) EditText mTxtSatu;

  private String mKey;
  private PageFragmentCallbacks mCallbacks;
  private FragmentOnePage mPage;

  public FragmentOnes create(String key) {
    Bundle args = new Bundle();
    args.putString(ARG_KEY, key);

    FragmentOnes fragment = new FragmentOnes();
    fragment.setArguments(args);
    return fragment;
  }

  public FragmentOnes() {
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Bundle args = getArguments();
    mKey = args.getString(ARG_KEY);
    mPage = (FragmentOnePage) mCallbacks.onGetPage(mKey);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_satu, container, false);
    //((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
    //((TextView) rootView.findViewById(R.id.wizard_text_field_desc)).setText(mPage.getDesc());

    //mFieldView = ((TextView) rootView.findViewById(R.id.wizard_text_field));
    //mFieldView.setHint(mPage.getTitle());
    //mFieldView.setText(mPage.getData().getString(Page.SIMPLE_DATA_KEY));

    ButterKnife.bind(this, rootView);
    return rootView;
  }

  @Override public void onDetach() {
    super.onDetach();
    mCallbacks = null;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mTxtSatu.addTextChangedListener(new TextWatcher() {
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }

      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }

      public void afterTextChanged(Editable editable) {
        mPage.getData()
            .putString(Page.SIMPLE_DATA_KEY, (editable != null) ? editable.toString() : null);
        mPage.notifyDataChanged();
      }
    });
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    if (!(activity instanceof PageFragmentCallbacks)) {
      throw new ClassCastException("Activity must implement PageFragmentCallbacks");
    }
    mCallbacks = (PageFragmentCallbacks) activity;
  }

  /*@OnTextChanged(value = R.id.txt_satu,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED) void afterTextInput(Editable editable) {
    mPage.getData()
        .putString(FragmentOnePage.NAME_DATA_KEY, (editable != null) ? editable.toString() : null);
    mPage.notifyDataChanged();
  }*/

  @Override public void setMenuVisibility(boolean menuVisible) {
    super.setMenuVisibility(menuVisible);
    if (mTxtSatu != null) {
      InputMethodManager imm =
          (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      if (!menuVisible) {
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
      }
    }
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
}
