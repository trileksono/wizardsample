package example.tri.dagger2rxjava.ui.wizard.ui;

import example.tri.dagger2rxjava.ui.wizard.model.Page;

/**
 * Created by tri on 2/20/17.
 */
public interface PageFragmentCallbacks {
  Page onGetPage(String key);
}
