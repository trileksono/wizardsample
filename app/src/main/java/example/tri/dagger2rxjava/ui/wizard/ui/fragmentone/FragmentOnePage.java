package example.tri.dagger2rxjava.ui.wizard.ui.fragmentone;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import example.tri.dagger2rxjava.ui.wizard.model.ModelCallbacks;
import example.tri.dagger2rxjava.ui.wizard.model.Page;
import example.tri.dagger2rxjava.ui.wizard.model.ReviewItem;
import java.util.ArrayList;

/**
 * Created by tri on 2/20/17.
 */

public class FragmentOnePage extends Page {

  public static final String NAME_DATA_KEY = "name";
  public static final String EMAIL_DATA_KEY = "email";

  public FragmentOnePage(ModelCallbacks callbacks, String title) {
    super(callbacks, title);
  }

  @Override public Fragment createFragment() {
    return new FragmentOne().create(getKey());
  }

  @Override public boolean isCompleted() {
    return !TextUtils.isEmpty(mData.getString(SIMPLE_DATA_KEY));
  }

  @Override public void getReviewItems(ArrayList<ReviewItem> dest) {
    dest.add(new ReviewItem("Your name", mData.getString(NAME_DATA_KEY), getKey(), -1));
    dest.add(new ReviewItem("Your email", mData.getString(EMAIL_DATA_KEY), getKey(), -1));
  }
}
