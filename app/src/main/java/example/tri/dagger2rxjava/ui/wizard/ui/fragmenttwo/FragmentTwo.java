package example.tri.dagger2rxjava.ui.wizard.ui.fragmenttwo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseFragment;

/**
 * Created by tri on 2/20/17.
 */

public class FragmentTwo extends BaseFragment {
  @Override protected int getResourceLayout() {
    return R.layout.activity_dua;
  }

  @Override protected void onViewReady(@Nullable Bundle savedInstanceState) {

  }
}
