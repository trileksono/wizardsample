package example.tri.dagger2rxjava.ui.stepper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import butterknife.Bind;
import butterknife.OnClick;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseActivity;
import example.tri.dagger2rxjava.base.BaseFragment;
import example.tri.dagger2rxjava.ui.fragment.FragmentDua;
import example.tri.dagger2rxjava.ui.fragment.FragmentSatu;
import example.tri.dagger2rxjava.util.StepPager;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tri on 2/15/17.
 */

public class StepperActivity extends BaseActivity {
  @Bind(R.id.pager) ViewPager pager;
  @Bind(R.id.indicator) StepPager indicator;
  @Bind(R.id.btn_prev) Button btnPrev;
  @Bind(R.id.btn_next) Button btnNext;

  MainAdapter adapter;
  private boolean mEditingAfterReview;
  private boolean mConsumePageSelectedEvent;
  private List<BaseFragment> mListFragment;

  @Override protected int getConctentView() {
    return R.layout.activity_main;
  }

  @Override protected void onViewReady(Bundle savedInstance) {
    mListFragment = Arrays.asList(new FragmentSatu(), new FragmentDua());
    adapter = new MainAdapter(getSupportFragmentManager(), mListFragment);
    pager.setAdapter(adapter);

    indicator.setOnPageSelectedListener(new StepPager.OnPageSelectedListener() {
      @Override public void onPageStripSelected(int position) {
        position = Math.min(adapter.getCount() - 1, position);
        if (pager.getCurrentItem() != position) {
          pager.setCurrentItem(position);
        }
      }
    });
    pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
      }

      @Override public void onPageSelected(int position) {
        indicator.setCurrentPage(position);

        if (mConsumePageSelectedEvent) {
          mConsumePageSelectedEvent = false;
          return;
        }

        mEditingAfterReview = false;
        updateBottomBar();
      }

      @Override public void onPageScrollStateChanged(int state) {
      }
    });

    onPageTreeChanged();
    updateBottomBar();
  }

  @OnClick(R.id.btn_next) void onNextClick() {
    if (pager.getCurrentItem() == mListFragment.size()) {
      DialogFragment dg = new DialogFragment() {
        @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
          return new AlertDialog.Builder(getActivity()).setMessage("Oke")
              .setPositiveButton("Oke", null)
              .setNegativeButton(android.R.string.cancel, null)
              .create();
        }
      };
      dg.show(getSupportFragmentManager(), "place_order_dialog");
    } else {
      if (mEditingAfterReview) {
        pager.setCurrentItem(adapter.getCount() - 1);
      } else {
        pager.setCurrentItem(pager.getCurrentItem() + 1);
      }
    }
  }

  private boolean recalculateCutOffPage() {
    // Cut off the pager adapter at first required page that isn't completed
    int cutOffPage = mListFragment.size() + 1;
    for (int i = 0; i < mListFragment.size(); i++) {
      Fragment page = mListFragment.get(i);
      //if (page.isRequired() && !page.isCompleted()) {
      //  cutOffPage = i;
      //  break;
      //}
    }

    if (adapter.getCutOffPage() != cutOffPage) {
      adapter.setCutOffPage(cutOffPage);
      return true;
    }

    return false;
  }

  private void onPageTreeChanged() {
    //mListFragment = adapter.getCurrentPageSequence();
    recalculateCutOffPage();
    indicator.setPageCount(mListFragment.size() + 1); // + 1 = review step
    adapter.notifyDataSetChanged();
    updateBottomBar();
  }

  private void updateBottomBar() {
    int position = pager.getCurrentItem();
    if (position == mListFragment.size()) {
      btnNext.setText("Finish");
      //btnNext.setBackgroundResource(R.drawable.finish_background);
    } else {
      btnNext.setText(mEditingAfterReview ? "Review" : "Next");
      //btnNext.setBackgroundResource(R.drawable.selectable_item_background);
      TypedValue v = new TypedValue();
      getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
      btnNext.setTextAppearance(this, v.resourceId);
      btnNext.setEnabled(position != adapter.getCutOffPage());
    }

    btnPrev.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
  }

  @OnClick(R.id.btn_prev) void onPrevClick() {
    pager.setCurrentItem(pager.getCurrentItem() - 1);
  }
}
