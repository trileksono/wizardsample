package example.tri.dagger2rxjava.ui.wizard.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseFragment;
import example.tri.dagger2rxjava.ui.wizard.model.AbstractWizardModel;
import example.tri.dagger2rxjava.ui.wizard.model.ModelCallbacks;
import example.tri.dagger2rxjava.ui.wizard.model.Page;
import example.tri.dagger2rxjava.ui.wizard.model.ReviewItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by tri on 2/20/17.
 */

public class LastFragment extends BaseFragment implements ModelCallbacks {
  private Callbacks mCallbacks;
  private AbstractWizardModel mWizardModel;
  private List<ReviewItem> mCurrentReviewItems;

  public LastFragment() {
  }

  @Override protected int getResourceLayout() {
    return R.layout.fragment_tiga;
  }

  @Override protected void onViewReady(@Nullable Bundle savedInstanceState) {

  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    Activity act = (Activity) context;
    if (!(act instanceof Callbacks)) {
      throw new ClassCastException("Activity must implement fragment's callbacks");
    }

    mCallbacks = (Callbacks) act;

    mWizardModel = mCallbacks.onGetModel();
    mWizardModel.registerListener(this);
    onPageTreeChanged();
  }

  @Override public void onDetach() {
    super.onDetach();
    mCallbacks = null;

    mWizardModel.unregisterListener(this);
  }

  @Override public void onPageDataChanged(Page pages) {
    ArrayList<ReviewItem> reviewItems = new ArrayList<ReviewItem>();
    for (Page page : mWizardModel.getCurrentPageSequence()) {
      page.getReviewItems(reviewItems);
    }
    Collections.sort(reviewItems, new Comparator<ReviewItem>() {
      @Override public int compare(ReviewItem a, ReviewItem b) {
        return a.getWeight() > b.getWeight() ? +1 : a.getWeight() < b.getWeight() ? -1 : 0;
      }
    });
    mCurrentReviewItems = reviewItems;
  }

  @Override public void onPageTreeChanged() {

  }

  public interface Callbacks {
    AbstractWizardModel onGetModel();
  }
}
