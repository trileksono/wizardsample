package example.tri.dagger2rxjava.ui.stepper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;
import example.tri.dagger2rxjava.base.BaseFragment;
import example.tri.dagger2rxjava.ui.fragment.FragmentTiga;
import java.util.List;

/**
 * Created by tri on 2/14/17.
 */

public class MainAdapter extends FragmentStatePagerAdapter {

  private int mCutOffPage;
  private List<BaseFragment> mCurrentPageSequence;
  private Fragment mPrimaryItem;

  public MainAdapter(FragmentManager fm, List<BaseFragment> listFragment) {
    super(fm);
    mCurrentPageSequence = listFragment;
  }

  @Override public int getCount() {
    if (mCurrentPageSequence == null) {
      return 0;
    }
    return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
  }

  @Override public Fragment getItem(int position) {
    if (position >= mCurrentPageSequence.size()) {
      return new FragmentTiga();
    }
    return mCurrentPageSequence.get(position);
  }

  @Override public int getItemPosition(Object object) {
    if (object == mPrimaryItem) {
      // Re-use the current fragment (its position never changes)
      return POSITION_UNCHANGED;
    }

    return POSITION_NONE;
  }

  @Override public void setPrimaryItem(ViewGroup container, int position, Object object) {
    super.setPrimaryItem(container, position, object);
    mPrimaryItem = (Fragment) object;
  }

  public void setCutOffPage(int cutOffPage) {
    if (cutOffPage < 0) {
      cutOffPage = Integer.MAX_VALUE;
    }
    mCutOffPage = cutOffPage;
  }

  public int getCutOffPage() {
    return mCutOffPage;
  }
}
