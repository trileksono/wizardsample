package example.tri.dagger2rxjava.ui.dua;

import android.os.Bundle;
import android.widget.Button;
import butterknife.Bind;
import butterknife.OnClick;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseActivity;
import example.tri.dagger2rxjava.base.BaseFragment;
import example.tri.dagger2rxjava.ui.fragment.FragmentDua;
import example.tri.dagger2rxjava.ui.fragment.FragmentSatu;
import example.tri.dagger2rxjava.ui.fragment.FragmentTiga;
import example.tri.dagger2rxjava.util.NoSwipeViewPager;
import java.util.Arrays;
import java.util.List;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by tri on 2/18/17.
 */

public class DuaActivity extends BaseActivity {
  @Bind(R.id.indicator) CircleIndicator indicator;
  @Bind(R.id.pager) NoSwipeViewPager pager;
  @Bind(R.id.btn_prev) Button btnPrev;
  @Bind(R.id.btn_next) Button btnNext;

  private DuaAdapter mAdapter;
  private List<BaseFragment> mListFragment;

  @Override protected int getConctentView() {
    return R.layout.activity_dua;
  }

  @Override protected void onViewReady(Bundle savedInstance) {
    mListFragment = Arrays.asList(new FragmentSatu(), new FragmentDua(), new FragmentTiga());
    mAdapter = new DuaAdapter(getSupportFragmentManager(), mListFragment);
    pager.setPagingEnabled(true);
    pager.setAdapter(mAdapter);
  }

  @OnClick(R.id.btn_next) void onNextClick() {
    if (mListFragment.size() == pager.getCurrentItem()) {
      showToast("You are on last fragment");
      return;
    }
    pager.setCurrentItem(pager.getCurrentItem() + 1);
  }

  @OnClick(R.id.btn_prev) void onPrevClick() {
    pager.setCurrentItem(pager.getCurrentItem() - 1);
  }
/*
  @Override protected void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Override protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Subscribe public void onValidationEvent(ValidationMessage msg) {
    Timber.e("Complete " + msg.isComplete());
    if (msg.isFirst()) {
      btnPrev.setVisibility(View.GONE);
    } else {
      btnPrev.setVisibility(View.VISIBLE);
    }
    if (msg.isComplete()) {
      btnNext.setEnabled(true);
    } else {
      btnNext.setEnabled(false);
    }
  }*/
}
