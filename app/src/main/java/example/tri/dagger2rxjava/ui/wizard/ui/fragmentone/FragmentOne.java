package example.tri.dagger2rxjava.ui.wizard.ui.fragmentone;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import butterknife.Bind;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseFragment;
import example.tri.dagger2rxjava.ui.wizard.ui.PageFragmentCallbacks;
import timber.log.Timber;

/**
 * Created by tri on 2/20/17.
 */

public class FragmentOne extends BaseFragment {
  private static final String ARG_KEY = "key";

  @Bind(R.id.txt_satu) EditText mTxtSatu;

  private String mKey;
  private PageFragmentCallbacks mCallbacks;
  private FragmentOnePage mPage;

  public FragmentOne create(String key) {
    Bundle args = new Bundle();
    args.putString(ARG_KEY, key);

    FragmentOne fragment = new FragmentOne();
    fragment.setArguments(args);
    return fragment;
  }

  public FragmentOne() {
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle args = getArguments();
    mKey = args.getString(ARG_KEY);
    mPage = (FragmentOnePage) mCallbacks.onGetPage(mKey);
  }

  @Override protected int getResourceLayout() {
    return R.layout.fragment_satu;
  }

  @Override protected void onViewReady(@Nullable Bundle savedInstanceState) {
    mTxtSatu.addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
        Timber.e(s.toString() + "");
      }

      @Override public void afterTextChanged(Editable s) {
        Timber.e("Change " + s.toString());
        mPage.getData().putString(FragmentOnePage.NAME_DATA_KEY, (s != null) ? s.toString() : null);
        mPage.notifyDataChanged();
      }
    });
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    Activity activity = (Activity) context;
    if (!(activity instanceof PageFragmentCallbacks)) {
      throw new ClassCastException("Activity must implement PageFragmentCallbacks");
    }
    mCallbacks = (PageFragmentCallbacks) activity;
  }

  /*@OnTextChanged(value = R.id.txt_satu,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED) void afterTextInput(Editable editable) {
    mPage.getData()
        .putString(FragmentOnePage.NAME_DATA_KEY, (editable != null) ? editable.toString() : null);
    mPage.notifyDataChanged();
  }*/

  @Override public void setMenuVisibility(boolean menuVisible) {
    super.setMenuVisibility(menuVisible);
    if (mTxtSatu != null) {
      InputMethodManager imm =
          (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      if (!menuVisible) {
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
      }
    }
  }
}
