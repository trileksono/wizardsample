package example.tri.dagger2rxjava.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import butterknife.Bind;
import example.tri.dagger2rxjava.R;
import example.tri.dagger2rxjava.base.BaseFragment;

/**
 * Created by tri on 2/15/17.
 */

public class FragmentTiga extends BaseFragment {

  @Bind(R.id.txt_tiga) EditText txtTiga;

  @Override protected int getResourceLayout() {
    return R.layout.fragment_tiga;
  }

  @Override protected void onViewReady(@Nullable Bundle savedInstanceState) {

  }
}
