package example.tri.dagger2rxjava.ui.wizard;

import android.content.Context;
import example.tri.dagger2rxjava.ui.wizard.model.AbstractWizardModel;
import example.tri.dagger2rxjava.ui.wizard.model.BranchPage;
import example.tri.dagger2rxjava.ui.wizard.model.PageList;
import example.tri.dagger2rxjava.ui.wizard.ui.fragmentone.FragmentOnePage;
import example.tri.dagger2rxjava.ui.wizard.ui.fragmenttwo.FragmentTwoPage;

/**
 * Created by tri on 2/20/17.
 */

public class WizardModel extends AbstractWizardModel {
  public WizardModel(Context context) {
    super(context);
  }

  @Override protected PageList onNewRootPageList() {
    return new PageList(new BranchPage(this, "Title").addBranch("Choice",
        new FragmentOnePage(this, "One").setRequired(true),
        new FragmentTwoPage(this, "Two").setRequired(true)).setRequired(true));
  }
}
