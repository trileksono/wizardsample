package example.tri.dagger2rxjava.ui.main;

import example.tri.dagger2rxjava.App;
import example.tri.dagger2rxjava.base.BasePresenter;
import example.tri.dagger2rxjava.data.LoginRequest;
import example.tri.dagger2rxjava.data.LoginResponse;
import example.tri.dagger2rxjava.net.API;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static example.tri.dagger2rxjava.util.Constant.URL_RUMAH_SEHAT;

/**
 * Created by tri on 2/11/17.
 */

public class MainPresenter implements BasePresenter<MainView> {

  private Subscription mSubscription;
  private MainView mView;
  private API mApi = App.getInstance().getApi(URL_RUMAH_SEHAT);

  void doLogin(LoginRequest mRequest) {
    mView.showLoading("Mohon tunggu ...");
    if (mSubscription != null) mSubscription.unsubscribe();
    mSubscription = mApi.doLogin(mRequest)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Subscriber<LoginResponse>() {
          @Override public void onCompleted() {
            mView.dismisLoading();
          }

          @Override public void onError(Throwable e) {
            mView.dismisLoading();
            mView.onErrorResponse("Error cuk");
          }

          @Override public void onNext(LoginResponse loginResponse) {
            mView.onResponse(loginResponse);
          }
        });
  }

  @Override public void onAttach(MainView mainView) {
    this.mView = mainView;
  }

  @Override public void onDettach() {
    if (mView != null) mView = null;
    if (mSubscription != null) mSubscription.unsubscribe();
  }
}
