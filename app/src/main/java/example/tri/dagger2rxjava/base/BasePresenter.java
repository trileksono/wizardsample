package example.tri.dagger2rxjava.base;

/**
 * Created by tri on 2/14/17.
 */

public interface BasePresenter<V> {

  void onAttach(V v);

  void onDettach();

}
