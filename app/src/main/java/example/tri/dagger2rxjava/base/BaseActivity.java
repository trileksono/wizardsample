package example.tri.dagger2rxjava.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.widget.Toast;
import butterknife.ButterKnife;

/**
 * Created by tri on 2/11/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

  protected Context mContext = this;
  protected LayoutInflater mInflater;
  private ProgressDialog mProgressDialog;
  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getConctentView());
    ButterKnife.bind(this);
    mInflater = LayoutInflater.from(this);
    onViewReady(savedInstanceState);
  }

  public FragmentManager getBaseFragmentManager() {
    return super.getSupportFragmentManager();
  }

  @Override public void onBackPressed() {
    if (getBaseFragmentManager().getBackStackEntryCount() > 0) {
      getBaseFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
    }
  }

  protected void showDialog(String message) {
    if (mProgressDialog == null) {
      mProgressDialog = new ProgressDialog(this);
      mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
      mProgressDialog.setCancelable(true);
    }
    mProgressDialog.setMessage(message);
    mProgressDialog.show();
  }

  protected void hideDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }

  protected void showToast(String message) {
    Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
  }

  protected abstract int getConctentView();

  protected abstract void onViewReady(Bundle savedInstance);
}
