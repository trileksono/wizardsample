package example.tri.dagger2rxjava.base;

import android.content.Context;

/**
 * Created by tri on 2/14/17.
 */

public interface BaseView {

  Context getContext();

  void showLoading(String message);

  void dismisLoading();
}
