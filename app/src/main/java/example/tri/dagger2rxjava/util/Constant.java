package example.tri.dagger2rxjava.util;

/**
 * Created by tri on 2/17/17.
 */

public class Constant {

  public static final String URL_RUMAH_SEHAT = "http://opendatav2.tangerangkota.go.id:8080/api/";

  public static final long UNIVERSAL_TIMEOUT = 15;
}
