package example.tri.dagger2rxjava.net;

import example.tri.dagger2rxjava.data.LoginRequest;
import example.tri.dagger2rxjava.data.LoginResponse;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by tri on 2/11/17.
 */

public interface API {

  @POST("loginPortal") Observable<LoginResponse> doLogin(@Body LoginRequest loginRequest);
}
