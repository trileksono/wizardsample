package example.tri.dagger2rxjava.net;

import android.content.Context;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static example.tri.dagger2rxjava.util.Constant.UNIVERSAL_TIMEOUT;

/**
 * Created by tri on 2/17/17.
 */

public class APIFactory {
  public API getApi(Context mContext, String URL) {
    return new Retrofit.Builder().baseUrl(URL)
        .client(mClient(mContext))
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .build()
        .create(API.class);
  }

  private OkHttpClient mClient(Context mContext) {
    Cache cache = null;
    try {
      cache = new Cache(new File(mContext.getCacheDir(), "http-cache"), 10 * 1024 * 1024); // 10 MB
    } catch (Exception e) {
      Timber.e(e, "Error creating cache file");
    }
    return new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor())
        //.addInterceptor(whenOfflineCacheInterceptor())
        //.addNetworkInterceptor(cacheInterceptor())
        //.addInterceptor(setCookie())
        //.addInterceptor(getCookie())
        .connectTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(UNIVERSAL_TIMEOUT, TimeUnit.SECONDS)
        .cache(cache)
        .build();
  }

  private HttpLoggingInterceptor httpLoggingInterceptor() {
    HttpLoggingInterceptor httpLoggingInterceptor =
        new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
          @Override public void log(String message) {
            Timber.d(message);
          }
        });
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return httpLoggingInterceptor;
  }

  /*public Interceptor setCookie() {
    return new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
          HashSet<String> cookies = new HashSet<>();

          for (String header : originalResponse.headers("Set-Cookie")) {
            cookies.add(header);
          }
          PrefUtil.setMapPref(Constant.PREF_SES, cookies);
        }

        return originalResponse;
      }
    };
  }

  public Interceptor getCookie() {
    return new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        HashSet<String> preferences = PrefUtil.getHashPref(Constant.PREF_SES);
        if (preferences != null) {
          for (String cookie : preferences) {
            builder.addHeader("Cookie", "JSESSIONID=EF91E3E30526CBDBF26BDACD26C4D7E9");//cookie);
          }
        }
        return chain.proceed(builder.build());
      }
    };
  }*/
}
