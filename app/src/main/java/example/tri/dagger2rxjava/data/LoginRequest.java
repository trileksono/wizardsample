package example.tri.dagger2rxjava.data;

/**
 * Created by tri on 2/11/17.
 */

public class LoginRequest {

  private String username;
  private String password;
  private String lat;
  private String lng;

  public LoginRequest(String username, String password, String lat, String lng) {
    this.username = username;
    this.password = password;
    this.lat = lat;
    this.lng = lng;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }
}
