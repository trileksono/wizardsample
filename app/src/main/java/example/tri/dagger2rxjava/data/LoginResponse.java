package example.tri.dagger2rxjava.data;

/**
 * Created by tri on 2/11/17.
 */

public class LoginResponse {
  private DataBean data;
  private boolean success;
  private String errorCode;
  private String message;

  public DataBean getData() {
    return data;
  }

  public void setData(DataBean data) {
    this.data = data;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public static class DataBean {
    private String gelarDepan;
    private String jabType;
    private String nomenklaturJabatan;
    private String gender;
    private String kodeUnor;
    private String idPegawaiSiap;
    private String nip;
    private String nomenklaturPada;
    private String idGroup;
    private String namaPegawai;
    private String posisi;
    private String idPegawaiSigap;
    private String namaUnor;
    private String idUnorSigap;
    private String group;
    private String idUnor;
    private String idKecamatan;
    private String unorSessStrip;
    private String gelarNonakademis;
    private String namaPangkat;
    private String namaGolongan;
    private String gelarBelakang;
    private String userId;
    private String statusKepegawaian;
    private String kodeGolongan;
    private String namaUnorSiap;
    private String tmtPns;
    private String idKelurahan;
    private String jenis;
    private String idPegawai;
    private String tugasTambahan;
    private String unorOpdStrip;
    private AksesMenuBean aksesMenu;
    private String akses;

    public String getGelarDepan() {
      return gelarDepan;
    }

    public void setGelarDepan(String gelarDepan) {
      this.gelarDepan = gelarDepan;
    }

    public String getJabType() {
      return jabType;
    }

    public void setJabType(String jabType) {
      this.jabType = jabType;
    }

    public String getNomenklaturJabatan() {
      return nomenklaturJabatan;
    }

    public void setNomenklaturJabatan(String nomenklaturJabatan) {
      this.nomenklaturJabatan = nomenklaturJabatan;
    }

    public String getGender() {
      return gender;
    }

    public void setGender(String gender) {
      this.gender = gender;
    }

    public String getKodeUnor() {
      return kodeUnor;
    }

    public void setKodeUnor(String kodeUnor) {
      this.kodeUnor = kodeUnor;
    }

    public String getIdPegawaiSiap() {
      return idPegawaiSiap;
    }

    public void setIdPegawaiSiap(String idPegawaiSiap) {
      this.idPegawaiSiap = idPegawaiSiap;
    }

    public String getNip() {
      return nip;
    }

    public void setNip(String nip) {
      this.nip = nip;
    }

    public String getNomenklaturPada() {
      return nomenklaturPada;
    }

    public void setNomenklaturPada(String nomenklaturPada) {
      this.nomenklaturPada = nomenklaturPada;
    }

    public String getIdGroup() {
      return idGroup;
    }

    public void setIdGroup(String idGroup) {
      this.idGroup = idGroup;
    }

    public String getNamaPegawai() {
      return namaPegawai;
    }

    public void setNamaPegawai(String namaPegawai) {
      this.namaPegawai = namaPegawai;
    }

    public String getPosisi() {
      return posisi;
    }

    public void setPosisi(String posisi) {
      this.posisi = posisi;
    }

    public String getIdPegawaiSigap() {
      return idPegawaiSigap;
    }

    public void setIdPegawaiSigap(String idPegawaiSigap) {
      this.idPegawaiSigap = idPegawaiSigap;
    }

    public String getNamaUnor() {
      return namaUnor;
    }

    public void setNamaUnor(String namaUnor) {
      this.namaUnor = namaUnor;
    }

    public String getIdUnorSigap() {
      return idUnorSigap;
    }

    public void setIdUnorSigap(String idUnorSigap) {
      this.idUnorSigap = idUnorSigap;
    }

    public String getGroup() {
      return group;
    }

    public void setGroup(String group) {
      this.group = group;
    }

    public String getIdUnor() {
      return idUnor;
    }

    public void setIdUnor(String idUnor) {
      this.idUnor = idUnor;
    }

    public String getIdKecamatan() {
      return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
      this.idKecamatan = idKecamatan;
    }

    public String getUnorSessStrip() {
      return unorSessStrip;
    }

    public void setUnorSessStrip(String unorSessStrip) {
      this.unorSessStrip = unorSessStrip;
    }

    public String getGelarNonakademis() {
      return gelarNonakademis;
    }

    public void setGelarNonakademis(String gelarNonakademis) {
      this.gelarNonakademis = gelarNonakademis;
    }

    public String getNamaPangkat() {
      return namaPangkat;
    }

    public void setNamaPangkat(String namaPangkat) {
      this.namaPangkat = namaPangkat;
    }

    public String getNamaGolongan() {
      return namaGolongan;
    }

    public void setNamaGolongan(String namaGolongan) {
      this.namaGolongan = namaGolongan;
    }

    public String getGelarBelakang() {
      return gelarBelakang;
    }

    public void setGelarBelakang(String gelarBelakang) {
      this.gelarBelakang = gelarBelakang;
    }

    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public String getStatusKepegawaian() {
      return statusKepegawaian;
    }

    public void setStatusKepegawaian(String statusKepegawaian) {
      this.statusKepegawaian = statusKepegawaian;
    }

    public String getKodeGolongan() {
      return kodeGolongan;
    }

    public void setKodeGolongan(String kodeGolongan) {
      this.kodeGolongan = kodeGolongan;
    }

    public String getNamaUnorSiap() {
      return namaUnorSiap;
    }

    public void setNamaUnorSiap(String namaUnorSiap) {
      this.namaUnorSiap = namaUnorSiap;
    }

    public String getTmtPns() {
      return tmtPns;
    }

    public void setTmtPns(String tmtPns) {
      this.tmtPns = tmtPns;
    }

    public String getIdKelurahan() {
      return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
      this.idKelurahan = idKelurahan;
    }

    public String getJenis() {
      return jenis;
    }

    public void setJenis(String jenis) {
      this.jenis = jenis;
    }

    public String getIdPegawai() {
      return idPegawai;
    }

    public void setIdPegawai(String idPegawai) {
      this.idPegawai = idPegawai;
    }

    public String getTugasTambahan() {
      return tugasTambahan;
    }

    public void setTugasTambahan(String tugasTambahan) {
      this.tugasTambahan = tugasTambahan;
    }

    public String getUnorOpdStrip() {
      return unorOpdStrip;
    }

    public void setUnorOpdStrip(String unorOpdStrip) {
      this.unorOpdStrip = unorOpdStrip;
    }

    public AksesMenuBean getAksesMenu() {
      return aksesMenu;
    }

    public void setAksesMenu(AksesMenuBean aksesMenu) {
      this.aksesMenu = aksesMenu;
    }

    public String getAkses() {
      return akses;
    }

    public void setAkses(String akses) {
      this.akses = akses;
    }

    public static class AksesMenuBean {
      /**
       * aksesSiapKemiskinan : true
       * aksesDashboard : false
       * aksesSigap : true
       * aksesTracking : false
       * aksesEabsensi : false
       * aksesSiapKelurahan : true
       * aksesSiapKecamatan : true
       * aksesToken : false
       * aksesAbsenThl : true
       * aksesSievlap : false
       */

      private boolean aksesSiapKemiskinan;
      private boolean aksesDashboard;
      private boolean aksesSigap;
      private boolean aksesTracking;
      private boolean aksesEabsensi;
      private boolean aksesSiapKelurahan;
      private boolean aksesSiapKecamatan;
      private boolean aksesToken;
      private boolean aksesAbsenThl;
      private boolean aksesSievlap;

      public boolean isAksesSiapKemiskinan() {
        return aksesSiapKemiskinan;
      }

      public void setAksesSiapKemiskinan(boolean aksesSiapKemiskinan) {
        this.aksesSiapKemiskinan = aksesSiapKemiskinan;
      }

      public boolean isAksesDashboard() {
        return aksesDashboard;
      }

      public void setAksesDashboard(boolean aksesDashboard) {
        this.aksesDashboard = aksesDashboard;
      }

      public boolean isAksesSigap() {
        return aksesSigap;
      }

      public void setAksesSigap(boolean aksesSigap) {
        this.aksesSigap = aksesSigap;
      }

      public boolean isAksesTracking() {
        return aksesTracking;
      }

      public void setAksesTracking(boolean aksesTracking) {
        this.aksesTracking = aksesTracking;
      }

      public boolean isAksesEabsensi() {
        return aksesEabsensi;
      }

      public void setAksesEabsensi(boolean aksesEabsensi) {
        this.aksesEabsensi = aksesEabsensi;
      }

      public boolean isAksesSiapKelurahan() {
        return aksesSiapKelurahan;
      }

      public void setAksesSiapKelurahan(boolean aksesSiapKelurahan) {
        this.aksesSiapKelurahan = aksesSiapKelurahan;
      }

      public boolean isAksesSiapKecamatan() {
        return aksesSiapKecamatan;
      }

      public void setAksesSiapKecamatan(boolean aksesSiapKecamatan) {
        this.aksesSiapKecamatan = aksesSiapKecamatan;
      }

      public boolean isAksesToken() {
        return aksesToken;
      }

      public void setAksesToken(boolean aksesToken) {
        this.aksesToken = aksesToken;
      }

      public boolean isAksesAbsenThl() {
        return aksesAbsenThl;
      }

      public void setAksesAbsenThl(boolean aksesAbsenThl) {
        this.aksesAbsenThl = aksesAbsenThl;
      }

      public boolean isAksesSievlap() {
        return aksesSievlap;
      }

      public void setAksesSievlap(boolean aksesSievlap) {
        this.aksesSievlap = aksesSievlap;
      }
    }
  }
}
