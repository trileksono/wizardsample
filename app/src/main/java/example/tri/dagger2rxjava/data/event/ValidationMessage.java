package example.tri.dagger2rxjava.data.event;

/**
 * Created by tri on 2/18/17.
 */

public class ValidationMessage {

  private boolean isFirst;
  private boolean isLast;
  private boolean isComplete;

  public ValidationMessage(boolean isFirst, boolean isLast, boolean isComplete) {
    this.isFirst = isFirst;
    this.isLast = isLast;
    this.isComplete = isComplete;
  }

  public boolean isFirst() {
    return isFirst;
  }

  public void setFirst(boolean first) {
    isFirst = first;
  }

  public boolean isLast() {
    return isLast;
  }

  public void setLast(boolean last) {
    isLast = last;
  }

  public boolean isComplete() {
    return isComplete;
  }

  public void setComplete(boolean complete) {
    isComplete = complete;
  }
}
